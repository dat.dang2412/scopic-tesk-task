<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BidController;
use App\Http\Controllers\AutoBiddingController;
use App\Http\Controllers\UserSettingController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);

Route::middleware(['jwt.auth'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->auth;
    });

    Route::put('/user/update-max-bid', [UserSettingController::class, 'updateMaxBidPrice']);

    Route::get('/products/{id}', [ProductController::class, 'show']);
    Route::get('/products/', [ProductController::class, 'showMany']);

    Route::post('/products/{id}/bids', [BidController::class, 'create']);
    Route::put('/products/{id}/auto-bids', [AutoBiddingController::class, 'createOrupdate']);
});