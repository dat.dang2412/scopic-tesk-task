<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Jobs\HandleAutoBiddingJob;

use App\Exceptions\ProductBidExpiredException;

/**
 * This is the model class for table "auto_biddings".
 *
 * The followings are the available columns in table 'auto_biddings':
 * @property integer $id
 * @property enum $enabled
 * @property integer $bidder_id
 * @property integer $product_id
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property User $bidder
 */
class AutoBidding extends Model {
    public const TEXT_YES = 'yes';
    public const TEXT_NO  = 'no';

    protected $table = 'auto_biddings';
    
    public function product () {
        return $this->belongsTo(Product::class);
    }

    public function bidder() {
        return $this->belongsTo(User::class);
    }

    /**
     * Create a new auto bidding instance or update an existing auto bidding instance.
     * @param User $bidder
     * @param Product $product
     * @param string $autoBiddingEnabled
     * @return AutoBidding $autoBidding
     */
    public static function createNewOrUpdate($bidder, $product, $autoBiddingEnabled) {
        $autoBidding = AutoBidding::where('bidder_id', $bidder->id)->where('product_id', $product->id)->first();

        if (empty($autoBidding)) {
            $autoBidding             = new AutoBidding();
            $autoBidding->bidder_id  = $bidder->id;
            $autoBidding->product_id = $product->id;
        }

        $autoBidding->enabled = $autoBiddingEnabled;

        if ($autoBidding->save() && $autoBidding->isEnabled()) {
            if ($product->isExpiredForBidding()) {
                $autoBidding->disableAutoBidding();
                throw new ProductBidExpiredException();
            }

            $autoBidding->dispatchAutoBiddingJob();
        }

        return $autoBidding;
    }

    /**
     * Dispatch the auto bidding job
     */
    public function dispatchAutoBiddingJob() {
        if ($this->isEnabled())
            HandleAutoBiddingJob::dispatch($this)
            ->delay(now()->addSeconds(10));
    }

    /**
     * Disable the auto bidding
     */
    public function disableAutoBidding() {
        $this->enabled = self::TEXT_NO;
        $this->save();
    }

    /**
     * Check if auto bidding is enabled
     */
    public function isEnabled() {
        return self::TEXT_YES === $this->enabled;
    }

    public static function checkIfAutoBiddingEnabledForProductAndBidder($bidder, $product) {
        $autoBidding = self::where('bidder_id', $bidder->id)
                        ->where('product_id', $product->id)
                        ->where('enabled', self::TEXT_YES)
                        ->first();

        return !empty($autoBidding);
    }
}
