<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property decimal $initial_price
 * @property decimal $current_price
 * @property datetime $end_date
 * @property string $image_url
 * @property integer $last_bidder_id
 * @property string $slug
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * The followings are the available model relations:
 * @property Bid $bids
 */
class Product extends Model {
    
    public function bids() {
        return $this->hasMany(Bid::class);
    }

    /**
     * Check if this product is expired for bidding
     * @return boolean
     */
    public function isExpiredForBidding() {
        $productExpireTime = new Carbon($this->end_date);
        $currentTime       = Carbon::now();

       return $currentTime > $productExpireTime;
    }

    /**
     * Check if the bid price is valid for this product
     * @return boolean
     */
    public function checkIfBidPriceValid($bidPrice) {
        $productCurrentPrice = $this->current_price;

        return $bidPrice > $productCurrentPrice;
    }

    /**
     * Update the price and the last bidder's id
     * @param integer $newPrice New price after bid
     * @param decimal $bidderId The id of the bidder
     */
    public function updatePriceAndBidder($newPrice, $bidderId) {
        $this->current_price  = $newPrice;
        $this->last_bidder_id = $bidderId;
        $this->save();
    }

    /**
     * Check if last bidder id of this product is the bidderId
     * @param integer $bidderId
     */
    public function checkIfUserIsTheLastBidder($bidderId) {
        return $bidderId == $this->last_bidder_id;
    }
}
