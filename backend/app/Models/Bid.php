<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Exceptions\BidAmountNotHigherThanLastBidException;
use App\Exceptions\ProductBidExpiredException;
use App\Exceptions\MustWaitForOtherBidException;

/**
 * This is the model class for table "bids".
 *
 * The followings are the available columns in table 'bids':
 * @property integer $id
 * @property decimal $bid_price
 * @property integer $bidder_id
 * @property integer $product_id
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property User $bidder
 */
class Bid extends Model {
    
    /**
     * Bid - Product belongs to relation
     */
    public function product () {
        return $this->belongsTo(Product::class);
    }

    public function bidder() {
        return $this->belongsTo(User::class);
    }

    /**
     * Add a new bid
     * @param User $bidder
     * @param Product $product
     * @param decimal $price bid price
     */
    public static function addNewBid($bidder, $product, $bidPrice) {
        if (!$product->checkIfBidPriceValid($bidPrice)) {
            throw new BidAmountNotHigherThanLastBidException();
        }

        if ($product->isExpiredForBidding()) {
            throw new ProductBidExpiredException();
        }

        if ($product->checkIfUserIsTheLastBidder($bidder->id)) {
            throw new MustWaitForOtherBidException();
        }

        $bid             = new Bid();
        $bid->bidder_id  = $bidder->id;
        $bid->product_id = $product->id;
        $bid->bid_price  = $bidPrice;

        if ($bid->save()) {
            $product->updatePriceAndBidder($bidPrice, $bidder->id);
            return $bid;
        }

        return null;
    }
}
