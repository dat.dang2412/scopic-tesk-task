<?php

namespace App\Jobs;

use App\Models\AutoBidding;
use App\Models\Bid;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\Middleware\WithoutOverlapping;

use App\Exceptions\BidAmountNotHigherThanLastBidException;
use App\Exceptions\ProductBidExpiredException;

class HandleAutoBiddingJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $autoBidding;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AutoBidding $autoBidding) {
        $this->autoBidding = $autoBidding;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $product            = $this->autoBidding->product;
        $bidder             = $this->autoBidding->bidder;
        $maxBidPrice        = $bidder->maximum_bid_amount;
        $newBidPrice        = $product->current_price  + 1; // last bid price plus one
        $updatedMaxBidPrice = $maxBidPrice - $newBidPrice;

        if (!$this->_validateAutoBidding($bidder, $product, $updatedMaxBidPrice)) return;

        try {
            if ($bid = Bid::addNewBid($bidder, $product, $newBidPrice)) {
                $bidder->updateMaxiumBidAmount($updatedMaxBidPrice);
            }
        } catch (BidAmountNotHigherThanLastBidException $exception) {
            return $this->autoBidding->dispatchAutoBiddingJob(); // new bid price is lower than the last bid, dispatch new job for re-bid
        } catch (ProductBidExpiredException $exception) {
            $this->autoBidding->disableAutoBidding();
            return;
        }

        $this->autoBidding->dispatchAutoBiddingJob();
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware() {
        return [
            (new WithoutOverlapping('bidding_'.$this->autoBidding->id)), // one auto bidding for a product at a time, will re-run
            (new WithoutOverlapping('user_'.$this->autoBidding->bidder->id)) // one auto bidding for the user at a time, will re-run
        ];
    }

    /**
     * Validate if we can do the bid
     * @param User $bidder
     * @param Product $product
     * @param decimal $updatedMaxBidPrice
     * @return bool 
     */
    private function _validateAutoBidding($bidder, $product, $updatedMaxBidPrice) {
        if (!$this->autoBidding->isEnabled()) return false;

        if ($product->checkIfUserIsTheLastBidder($bidder->id)) {  // last bidder, wait for the others to bid
            $this->autoBidding->dispatchAutoBiddingJob(); // re-bid
            return false;
        }

        if ($updatedMaxBidPrice < 0) { // no more funds, stop the bid
            $this->autoBidding->disableAutoBidding();
            return false;
        }

        return true;
    }
}
