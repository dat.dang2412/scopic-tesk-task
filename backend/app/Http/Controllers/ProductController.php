<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\AutoBidding;

use App\Http\ResponseCode\ProductCode;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller {
   
    /**
     * Show the detail of a product
     * @param Request $request
     * @param integer $productId
     */
    public function show(Request $request, $productId) {
        try {
            $product = Product::with(['bids' => function ($q) {
                            $q->with('bidder')->orderBy('created_at', 'desc')->take(5);
                        }])
                        ->findOrFail($productId);
        } catch(ModelNotFoundException $e) {
            return $this->returnResponseWithMessageAndStatusCode(ProductCode::NOT_FOUND, 'Product does not exist.', 404);
        }

        $product->enabled_auto_bidding = AutoBidding::checkIfAutoBiddingEnabledForProductAndBidder($request->auth, $product);

        return $product;
    }

    /**
     * Show many products using pagination
     */
    public function showMany(Request $request) {
        $this->validate($request, [
            'order'   => 'in:desc,asc'
        ]);

        $keyword = $request->keyword;
        $order   = $request->order;

        return Product::when(!empty($keyword) && strlen($keyword) > 0, function($q) use ($keyword) {
                    $q->where('name', 'like', "%$keyword%")->orWhere('description', 'like', "%$keyword%");
                })
                ->when(!empty($order), function ($q) use ($order) {
                    $q->orderBy('current_price', $order);
                })
                ->whereDate('end_date', '>', Carbon::now())
                ->withCount('bids')
                ->paginate(8);
    }
}
