<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserSettingController extends Controller {

    /**
     * Update the max bid price/amount of current logged user
     */
    public function updateMaxBidPrice(Request $request) {
        $this->validate($request, [
            'max_bid_price' => 'required|max:12|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/'
        ]);

        $user                     = $request->auth;
        $user->maximum_bid_amount = $request->max_bid_price;
        $user->save();

        return $user->maximum_bid_amount;
    }
}