<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return general format of response for controller
     * @param boolean $success
     * @param string $code
     * @param string $message
     * @param integer $httpStatusCode
     */
    public function returnResponseWithMessageAndStatusCode($code, $message, $httpStatusCode) {
        return response()->json([
            'success' => false,
            'code'    => $code,
            'detail'  => $message
        ], $httpStatusCode); 
    }
}
