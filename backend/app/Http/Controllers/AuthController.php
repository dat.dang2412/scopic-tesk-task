<?php

namespace App\Http\Controllers;

use App\Models\User;

use App\Http\ResponseCode\AuthCode;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthController extends Controller {
    protected function jwt(User $user) {
        $payload = [
            'iss' => "laravel-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60*3600 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function login(Request $request) {
        $this->validate($request, [
            'username'  => 'required|min:4|max:15',
            'password'  => 'required'
        ]);

        $this->_createDummyUser($request->username, $request->password); // hardcode for testing

        $user = null;
        // Find the user by username
        try {
            $user = User::where('username', $request->input('username'))->firstOrFail();
        } catch(ModelNotFoundException $e){
            return $this->returnResponseWithMessageAndStatusCode(AuthCode::USER_NOT_FOUND, 'User does not exist.', 404);
        }

        // Verify the password and generate the token
        if (Hash::check($request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user),
                'user'  => $user
            ], 200);
        }

        // Bad Request response
        return response()->json([
            'success'=> false,
            'message' => 'Username or password is wrong.',
            'code'    => AuthCode::AUTHENTICATION_FAILED
        ], 400);
    }

    /**
     * Hard code for testing
     * @param string $username
     * @param string $password
     */
    private function _createDummyUser($username, $password) {
        if ($username === 'user1' && $password === 'user2') {
            $user = User::where('username', $username)->first();
            if (empty($user)) {
                $user                     = new User();
                $user->username           = 'user1';
                $user->password           = Hash::make('user2');
                $user->name               = 'Admin';
                $user->maximum_bid_amount = 1000;
                $user->save();
            }
        }
    }
}
