<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\AutoBidding;

use App\Http\ResponseCode\ProductCode;
use App\Http\ResponseCode\AutoBiddingCode;

use Illuminate\Http\Request;

class AutoBiddingController extends Controller {
 
    /**
     * Enable/disable auto bidding for a product
     * @param Request $request
     * @param $productId product's id
     * @return AutoBidding @autobidding
     */
    public function createOrupdate(Request $request, $productId) {
        $this->validate($request, [
            'enabled' => 'required|in:yes,no'
        ]);

        $product            = null;
        $autoBidding        = null;
        $bidder             = $request->auth;
        $autoBiddingEnabled = $request->enabled;

        try {
            $product = Product::findOrFail($productId);
        } catch(ModelNotFoundException $e) {
            return $this->returnResponseWithMessageAndStatusCode(ProductCode::NOT_FOUND, 'Product does not exist.', 404);
        }

        try {
            $autoBidding = AutoBidding::createNewOrUpdate($bidder, $product, $autoBiddingEnabled);
        } catch (ProductBidExpiredException $exception) {
            return $this->returnResponseWithMessageAndStatusCode(ProductCode::PRODUCT_BID_EXPIRED, 'The bid process for this product is no longer opened.', 400);
        }

        if (empty($autoBidding)) {
            return $this->returnResponseWithMessageAndStatusCode(AutoBiddingCode::FAIL_TO_ENABLE_OR_DISABLE, 'Failed to enable or disable the auto bidding on this product.', 500);
        }

        return $autoBidding;
    }
}
