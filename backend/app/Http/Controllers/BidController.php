<?php

namespace App\Http\Controllers;

use App\Models\Bid;
use App\Models\User;
use App\Models\Product;

use Illuminate\Http\Request;
use App\Http\ResponseCode\ProductCode;
use App\Http\ResponseCode\BidCode;

use Illuminate\Database\Eloquent\ModelNotFoundException;  
use App\Exceptions\BidAmountNotHigherThanLastBidException;
use App\Exceptions\ProductBidExpiredException;
use App\Exceptions\MustWaitForOtherBidException;

class BidController extends Controller {
 
    /**
     * Create a new bid
     * @param Request $request
     * @param integer $productId product's id
     * @return Bid $bid 
     */
    public function create(Request $request, $productId) {
        $this->validate($request, [
            'bid_price' => 'required|max:12|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/'
        ]);

        $product   = null;
        $bid       = null;
        $bidder  = $request->auth;
        $bidPrice  = $request->bid_price;

        try {
            $product = Product::findOrFail($productId);
        } catch(ModelNotFoundException $e) {
            return $this->returnResponseWithMessageAndStatusCode(ProductCode::NOT_FOUND, 'Product does not exist.', 404);
        }
        
        try {
            $bid = Bid::addNewBid($bidder, $product, $bidPrice);
        } catch (BidAmountNotHigherThanLastBidException $exception) {
            return $this->returnResponseWithMessageAndStatusCode(BidCode::BID_AMOUNT_NOT_HIGHER_THAN_LAST_BID, 'The bid price must be higher than the last bid price.', 400);
        } catch (ProductBidExpiredException $exception) {
            return $this->returnResponseWithMessageAndStatusCode(ProductCode::PRODUCT_BID_EXPIRED, 'The bid process for this product is no longer opened.', 400);
        } catch (MustWaitForOtherBidException $exception) {
            return $this->returnResponseWithMessageAndStatusCode(BidCode::WAIT_FOR_OTHER_BID, 'You must wait for the others to bid!', 400);
        }

        if (empty($bid)) {
            return $this->returnResponseWithMessageAndStatusCode(BidCode::BID_FAILED, 'Bid failed.', 500);
        }

        return $bid;
    }
}
