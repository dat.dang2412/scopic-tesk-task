<?php

namespace App\Http\ResponseCode;

class ProductCode {
    public const NOT_FOUND = 'PRODUCT_NOT_FOUND';

    public const PRODUCT_BID_EXPIRED = 'PRODUCT_BID_EXPIRED';
}