<?php

namespace App\Http\ResponseCode;

class AuthCode {
    public const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';

    public const USER_NOT_FOUND = 'USER_NOT_FOUND';
}