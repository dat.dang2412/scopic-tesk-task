<?php

namespace App\Http\ResponseCode;

class BidCode {
    public const BID_AMOUNT_NOT_HIGHER_THAN_LAST_BID = 'BID_AMOUNT_NOT_HIGHER_THAN_LAST_BID';

    public const BID_FAILED = 'BID_FAILED';

    public const WAIT_FOR_OTHER_BID = 'WAIT_FOR_OTHER_BID';
}