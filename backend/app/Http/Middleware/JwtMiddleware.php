<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->get('token');

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'success'   => false,
                'error'     => 'Token not provided.',
                'code'      => 'TOKEN_NOT_PROVIDED'
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'success'   => false,
                'error'     => 'Provided token is expired.',
                'code'      => 'TOKEN_EXPIRED'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'success'   => false,
                'error'     => 'An error while decoding token.',
                'code'      => 'INVALID_TOKEN'
            ], 400);
        }

        $user = User::find($credentials->sub);

        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;

        return $next($request);
    }
}
