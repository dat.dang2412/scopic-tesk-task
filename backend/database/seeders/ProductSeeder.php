<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Str;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');
        \DB::beginTransaction();
            try {
                foreach (range(1, 100) as $index) {
                    \DB::table('products')->insert([
                        'name'          => $faker->name,
                        'description'   => $faker->paragraph,
                        'initial_price' => $faker->randomDigit,
                        'current_price' => $faker->randomDigit + 20,
                        'image_url'     => 'https://picsum.photos/seed/picsum/200/150',
                        'end_date'      => $faker->randomElement(['2021-04-12 12:55:30','2021-04-13 14:22:30', '2021-04-14 12:22:30', '2021-04-13 8:22:30', '2021-04-13 7:22:30', '2021-04-15 18:20:32']),
                        'slug'          => Str::slug($faker->name. uniqid(), '-')
                        ]);
                    }
                \DB::commit();
            } catch (Exception $e) {
                \DB::rollback();
            }

    }
}
