# Scopic test task - Dat Dang
dat.dang2412@gmail.com
This test task project use Laravel as backend and ReactJS as frontend.


## Installation & testing
### Backend


From the project root, run the following command to bring up containers 

``
cd backend && ./vendor/bin/sail up
``

Migrate the database:

``
docker exec -it backend_laravel.test_1 php /var/www/html/artisan migrate
``

Next, seed the product table with the following command to create dummy data:

``
docker exec -it backend_laravel.test_1 php /var/www/html/artisan db:seed --class=ProductSeeder
``

Then run the laravel queue for queuing jobs:

``
docker exec -it backend_laravel.test_1 php /var/www/html/artisan queue:work &
``
### Frontend
To run frontend environment, from the project root run:
``
cd frontend && yarn start
``

Backend environtment will be running at: http://localhost:8021/

Frontend enviroment  will be running at: http://localhost:3000/ 

Default login credentials: user1/user2