import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import 'antd/dist/antd.css';

import {
  BrowserRouter as Router
} from "react-router-dom";
import { Layout } from 'antd';
import { Provider } from 'react-redux';

import Routes from './components/Routes';
import store from './redux/store';
import Header from './components/Header'

const { Content } = Layout;

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Layout>
          <Header/>
          <Content style={{ padding: '30px 20px' }}>
            <Routes />
          </Content>
        </Layout>
      </Router>
    </Provider>
  );
}

export default App;
