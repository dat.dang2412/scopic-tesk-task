import React from 'react'
import {
    Route,
    Redirect,
    Switch
} from "react-router-dom";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Home, ProductDetail, Login } from '../pages/index';
import { useState } from "react";

const Routes = (props) => {
    const [isLoggedIn, setIsLoggedIn] = useState(props.isLoggedIn);

    const PrivateRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
            isLoggedIn === true
                ? <Component {...props} />
                : <Redirect to='/login' />
        )} />
    )

    const PublicRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
            !isLoggedIn
                ? <Component {...props} />
                : <Redirect to='/' />
        )} />
    )

    return (
        <Switch>
            <PublicRoute path="/login" component={Login} />
            <PrivateRoute path="/product-detail" component={ProductDetail} />
            <PrivateRoute path="/" component={Home} />
        </Switch >
    )
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.loginPageReducer.isLoggedIn,
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Routes);