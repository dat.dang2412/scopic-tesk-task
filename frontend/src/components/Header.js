import { connect } from 'react-redux';
import { Layout, Col, Row, Button, Modal, Input } from 'antd';
import { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { ToastContainer, toast } from 'react-toastify';

import { getAuthUser } from '../services/authServices';
import { updateMaxBidPrice } from '../services/userServices';

const Header = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [maxBidPrice, setMaxBidPrice] = useState(0);

    useEffect(() => {
        if (props.isLoggedIn) {
            props.getAuthUser().then(responseData => {
                setMaxBidPrice(responseData.user?.maximum_bid_amount)
            })
        }
    }, [])

    const onOk = () => {
        props.updateMaxBidPrice(maxBidPrice).then(responseData => {
            setMaxBidPrice(responseData.price)
            if (responseData.success) {
                toast.success(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                })
                setIsModalVisible(false);
            } else {
                toast.error(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                })
            }
        })
    }

    const onCancel = () => {
        setIsModalVisible(false);
        setMaxBidPrice(props.user?.maximum_bid_amount)
    }

    return (
        <>
            <Layout.Header style={{ background: '#fff' }}>
                <Row>
                    <Col span={12}><h1>Scopic Test task</h1></Col>
                    {
                        props.isLoggedIn && <Col span={12} style={{ textAlign: 'right' }}>
                            <Button onClick={() => setIsModalVisible(true)}>Update Max bid Price</Button>
                        </Col>
                    }
                </Row>
            </Layout.Header>
            <Modal title="Change max bid price" visible={isModalVisible} onOk={onOk} onCancel={onCancel}>
                <span>Max bid price:</span>
                <Input onChange={(e) => {
                    setMaxBidPrice(e.target.value)
                }} value={maxBidPrice}/>
            </Modal>
            <ToastContainer/>
        </>
    )
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.loginPageReducer.isLoggedIn,
        user: state.loginPageReducer.user
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getAuthUser,
    updateMaxBidPrice
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);