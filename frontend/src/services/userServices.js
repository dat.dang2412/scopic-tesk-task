import request from './request';
import routes from './routes';

export const updateMaxBidPrice = (newPrice) => {
    return dispatch => {
        return request.put(routes.USER.UPDATE_MAX_BID_AMOUNT, {
            max_bid_price: newPrice
        }).then(res => {
            return {
                success: true,
                price: res.data,
                message: 'Max bid price updated!'
            }
        }).catch(error => {
            return {
                success: false,
                price: newPrice,
                message: error.response?.data?.message
            }
        })
    }
}