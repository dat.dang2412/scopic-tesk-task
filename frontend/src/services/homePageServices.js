import request from './request';
import routes from './routes';

export const getProducts = (page, keyword, sortOrder) => {
    return dispatch => {
        return request.get(routes.PRODUCT.GET, {
            params: {
                page: page,
                keyword: keyword,
                order: sortOrder
            }
        }).then(res => {
            return {
                success: true,
                data: res.data
            }
        }).catch(error => {
            return {
                success: false,
                data: error
            }
        })
    }
}
