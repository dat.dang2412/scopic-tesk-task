import request from './request';
import routes from './routes';

export const bid = (bidPrice, product) => {
    return dispatch => {
        return request.post(routes.PRODUCT.BID(product.id), {
            bid_price: bidPrice,
        }).then(async res => {
            return {
                success: true,
                message: 'Bid sucessfully!'
            }
        }).catch(error => {
            return {
                success: false,
                message: error.response?.data?.detail
            }
        })
    }
}

export const getProduct = (productId) => {
    return dispatch => {
        return request.get(routes.PRODUCT.GET + productId).then(res => {
            return {
                success: true,
                data: res.data
            }
        }).catch(error => {
            return {
                success: false,
                data: error
            }
        })
    }
}

export const autoBidding = (enabled, productId) => {
    return dispatch => {
        return request.put(routes.PRODUCT.AUTO_BID(productId), {
            enabled: enabled
        }).then(res => {
            return {
                success: true,
                message: 'Auto-bidding has been ' + ( enabled === 'yes' ? 'enabled' : 'disabled') + ' for this product.'
            }
        }).catch(error => {
            return {
                success: false,
                message: error.response?.data?.detail
            }
        })
    }
}