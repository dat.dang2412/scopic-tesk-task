const APP_URL = 'http://localhost:8021';

const routes = {
    PRODUCT: {
        GET: `${APP_URL}/api/products/`,
        BID: (id)=> `${APP_URL}/api/products/${id}/bids`,
        AUTO_BID: (id)=> `${APP_URL}/api/products/${id}/auto-bids`,
    },

    LOGIN: `${APP_URL}/api/login`,

    USER: {
        GET_INFO: `${APP_URL}/api/user`,
        UPDATE_MAX_BID_AMOUNT: `${APP_URL}/api/user/update-max-bid`
    }
}

export default routes