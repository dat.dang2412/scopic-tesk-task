import { setLoggedIn, setUser } from '../redux/actions/loginPageActions';
import request from './request';
import routes from './routes';

export const login = ({ username, password }) => {
    return dispatch => {
        return request.post(routes.LOGIN, {
            username: username,
            password: password
        }).then(async res => {
            if (res.data?.token) {
                localStorage.setItem('access_token', res.data.token)
                dispatch(setUser(res.data.user))
                dispatch(setLoggedIn(true))
            }

            return {
                success: true,
                data: res.data
            }
        }).catch(error => {
            return {
                success: false,
                data: error?.response?.data,
                message: error?.response?.data?.detail
            }
        })
    }
}

export const getAuthUser = () => {
    return dispatch => {
        return request.get(routes.USER.GET_INFO, {
        }).then(async res => {
            dispatch(setUser(res.data))
            return {
                success: true,
                user: res.data
            }
        }).catch(error => {
            return {
                success: false,
                user: null
            }
        })
    }
}
