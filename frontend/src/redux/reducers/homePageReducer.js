import { type as actions } from '../actions/homePageActions'

const initData = {
    productData: {}
}

const homePageReducer = (state = initData, { type, payload }) => {
    switch (type) {
        case actions.SET_PRODUCTS_RESPONSE:
            return {
                ...state,
                productData: payload
            }
        default:
            return state;
    }
}

export default homePageReducer
