import { combineReducers } from 'redux';

import homePageReducer from './homePageReducer';
import loginPageReducer from './loginPageReducer';

const rootReducer = combineReducers({
    homePageReducer,
    loginPageReducer,
});

export default rootReducer;
