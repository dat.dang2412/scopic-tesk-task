import { type as actions } from '../actions/loginPageActions'

const initData = {
    loginData: {
        username: '',
        password: ','
    },
    user: {},
    isLoggedIn: !!localStorage.getItem('access_token')
}

const loginPageReducer = (state = initData, { type, payload }) => {
    switch (type) {
        case actions.SET_LOGIN_PARAMS:
            return {
                ...state,
                loginData: payload
            }
        case actions.SET_LOGGED_IN:
            return {
                ...state,
                isLoggedIn: payload
            }
        case actions.SET_USER:
            return {
                ...state,
                user: payload
            }
        default:
            return state;
    }
}

export default loginPageReducer
