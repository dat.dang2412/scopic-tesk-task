export const type = {
    SET_LOGIN_PARAMS: 'SET_PICKER_TYPE',
    SET_LOGGED_IN: 'SET_LOGGED_IN',
    SET_USER: 'SET_USER'
}

export const setLoginParams = (payload) => ({
    type: type.SET_LOGIN_PARAMS,
    payload
})

export const setLoggedIn = (payload) => ({
    type: type.SET_LOGGED_IN,
    payload
})

export const setUser = (payload) => ({
    type: type.SET_USER,
    payload
})