export const type = {
    SET_PRODUCTS_RESPONSE: 'SET_PRODUCTS_RESPONSE',
}

export const setProductsResponse = (payload) => ({
    type: type.SET_PRODUCTS_RESPONSE,
    payload
})