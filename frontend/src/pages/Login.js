import { useState } from 'react';
import { useHistory } from "react-router-dom";

import { Form, Input, Button, Row, Col, PageHeader, Divider } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';

import { login } from '../services/authServices';

const Login = (props) => {
    const history = useHistory();
    const [responseError, setResponseError] = useState({});

    const onFinish = (values) => {
        props.login(values).then(responseData => {
            if (responseData.success) {
                window.location.reload()
            } else {
                responseData.message && toast.error(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                })
                setResponseError(responseData.data)
            }
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <ToastContainer />
            <PageHeader
                className="site-page-header"
                title="Login"
                subTitle=""
            />
            <Row gutter="16" type="flex" justify="center" align="middle">
                <Col xs={{ span: 32 }} lg={{ span: 10 }} >
                    <Form
                        name="basic"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            hasFeedback={!!responseError?.errors?.username[0]}
                            validateStatus={!!responseError?.errors?.username[0] ? 'error' : ''}
                            help={!!responseError?.errors?.username[0] ? responseError?.errors?.username[0] : ''}
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },

                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Divider orientation="left"></Divider>
                        <Row type="flex" justify="end">
                            <Col style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit">Login</Button>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </>
    );
};

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    // setLoginParams
    login
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);