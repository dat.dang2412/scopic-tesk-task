import { useEffect, useState } from 'react';
import { List, Card, Row, Button, Pagination, AutoComplete, PageHeader, Breadcrumb, Switch } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Countdown from 'react-countdown';
import { useHistory } from "react-router-dom";

import { getProducts } from '../services/homePageServices';

const sortOrderConstant = {
    desc: 'desc',
    asc: 'asc'
}

const Home = (props) => {
    const history = useHistory();
    const [productData, setProductData] = useState({});
    const [searchKeyword, setSearchKeyword] = useState('');
    const [sortOrder, setSortOrder] = useState(sortOrderConstant.desc);

    useEffect(() => {
        getProducts(1, searchKeyword, sortOrder)
        // eslint-disable-next-line
    }, [])

    const getProducts = (page, keyword = '', order = sortOrderConstant.desc) => {
        props.getProducts(page, keyword, order).then(responseData => {
            if (responseData.success) {
                setProductData(responseData.data)
            }
        })
    }

    const onPaginationChanged = (page, pageSize) => {
        getProducts(page, searchKeyword, sortOrder)
    }

    const onSearch = (value) => {
        setSearchKeyword(value)
        getProducts(1, value, sortOrder)
    }

    const onChangeSortOrder = (checked) => {
        let order = checked ? sortOrderConstant.asc : sortOrderConstant.desc
        setSortOrder(order)
        getProducts(1, searchKeyword, order)
    }

    return (
        <>
            <PageHeader
                className="site-page-header"
                title="Products"
                subTitle="Browse products"
                extra={[
                    <AutoComplete
                        style={{ width: 200 }}
                        onSearch={onSearch}
                        placeholder="Search product by name and description"
                    />,
                    <><span>Price order</span> <Switch onChange={onChangeSortOrder} checkedChildren={sortOrderConstant.asc} unCheckedChildren={sortOrderConstant.desc} /></>
                ]}
            >
                <Breadcrumb>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
            </PageHeader>

            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 4,
                }}
                dataSource={productData.data ? productData.data : []}
                renderItem={(item, index) => (
                    <List.Item key={index}>
                        <div style={{ background: '#fff', borderRadius: '5px' }}>
                            <Row type="flex" justify="space-between" style={{ padding: '10px' }}>
                                <div><b>{item.bids_count}</b> bids</div>
                                <b><Countdown daysInHours={true} date={item.end_date} /></b>
                            </Row>
                            <Card cover={<img alt={'cover'} height={'auto'} width={'100%'} src={item.image_url} />}>
                                <h3>{item.name}</h3>
                                <h4>{item.description}</h4>
                                <h3>Initial Price: ${item.initial_price}</h3>
                                <h3>Last Bid Price: ${item.current_price}</h3>
                                <Button style={{ width: '100%' }} onClick={() => {
                                    history.push({
                                        pathname: '/product-detail',
                                        search: '?product_id=' + item.id,
                                        state: { item_id: item.id }
                                    })
                                }}>Bid now</Button>
                            </Card>
                        </div>
                    </List.Item>
                )}
            />
            <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
                <Pagination defaultCurrent={1} pageSize={8} onChange={onPaginationChanged} defaultPageSize={8} current={productData.current_page} total={productData.total} />
            </Row>
        </>

    )
}

const mapStateToProps = state => {
    return {
        // 
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);