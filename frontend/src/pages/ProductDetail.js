import { useEffect, useState } from 'react';

import { Descriptions, Image, Row, Col, Card, Button, Form, List, Breadcrumb, PageHeader, InputNumber, Switch } from 'antd';
import { useLocation } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Countdown from 'react-countdown';
import moment from 'moment'
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';

import { getProduct, bid, autoBidding } from '../services/productDetailPageService'

const ProductDetail = (props) => {
    const history = useHistory();
    const params = useLocation();
    const [product, setProduct] = useState({});
    const [enableAutoBidding, setEnabledAutoBidding] = useState(false);

    useEffect(() => {
        getProduct()
    }, [])

    const getProduct = () => {
        props.getProduct(params.state.item_id).then(responseData => {
            if (responseData.success) {
                setProduct(responseData.data)
                setEnabledAutoBidding(responseData.data?.enabled_auto_bidding)
            }
        })
    }

    const onFinish = ({ bid_price }) => {
        props.bid(bid_price, product).then(responseData => {
            if (responseData.success) {
                getProduct()
                toast.success(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                !!responseData.message && toast.error(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    const toggleAutoBidding = (checked) => {
        setEnabledAutoBidding(checked)
        props.autoBidding(checked ? 'yes' : 'no', product.id).then(responseData => {
            if (responseData.success) {
                toast.info(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                })
            } else {
                toast.error(responseData.message, {
                    position: toast.POSITION.TOP_RIGHT
                })
            }
        })
    }

    return (
        <>
            <ToastContainer />
            <PageHeader
                className="site-page-header"
                title={'Product: ' + product.name}
                subTitle=""
                extra={[
                    <Button onClick={() => {
                        history.push('/')
                    }}>Back</Button>
                ]}
            >
                <Breadcrumb>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>Product</Breadcrumb.Item>
                    <Breadcrumb.Item>{product.name}</Breadcrumb.Item>
                </Breadcrumb>
            </PageHeader>
            <Card>
                <Row>
                    <Col xs={{ span: 32 }} lg={{ span: 12 }}><Image src="https://cdn.nguyenkimmall.com/images/detailed/697/10047666-dien-thoai-iphone-12-mini-64gb-xanh-duong-1.jpg" /></Col>
                    <Col xs={{ span: 32 }} lg={{ span: 12 }} style={{paddingLeft: '10px'}}>
                        <Row>
                            <Descriptions title={product.name} extra={<><span>Auto bidding</span> <Switch checked={enableAutoBidding} onChange={toggleAutoBidding} checkedChildren="Enabled" unCheckedChildren="Disabled" /></>}>
                                <Descriptions.Item label="Initial price">${product.initial_price}</Descriptions.Item>
                                <Descriptions.Item label="Last bid price">${product.current_price}</Descriptions.Item>
                                <Descriptions.Item label="Time Remaining">
                                    <Countdown date={new Date(product.end_date)} daysInHours={true} />
                                </Descriptions.Item>
                                <Descriptions.Item label="Description">
                                    {product.description}
                                </Descriptions.Item>
                            </Descriptions>
                        </Row>

                        <Form
                            wrapperCol={{ span: 24 }}
                            name="basic"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Row justify="center">
                                <Form.Item
                                    wrapperCol={{
                                        span: 24
                                    }}
                                    name="bid_price"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input a number',
                                            type: 'number',
                                        },
                                    ]}
                                >
                                    <InputNumber placeholder={`${Math.floor(product.current_price) + 1}`} />
                                </Form.Item>
                            </Row>
                            <Row justify="center">
                                <Form.Item>
                                    <Button htmlType="submit" type="primary">Bid now</Button>
                                </Form.Item>
                            </Row>
                        </Form>

                        <h2>Bid history</h2>
                        <List
                            itemLayout="horizontal"
                            dataSource={product.bids}
                            renderItem={(item, index) => (
                                <List.Item>
                                    <List.Item.Meta
                                        title={(index + 1) + '. ' + item.bidder?.name}
                                        description={` Last bid $${item.bid_price} at ${moment(item.created_at).format('YYYY-MM-DD, HH:mm:ss')}`}
                                    />
                                </List.Item>
                            )}
                        />
                    </Col>
                </Row>
            </Card >
        </>
    )
}

const mapStateToProps = state => {
    return {
        // 
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getProduct,
    bid,
    autoBidding
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductDetail);