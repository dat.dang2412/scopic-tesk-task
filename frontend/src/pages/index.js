import Home from './Home'
import Login from './Login'
import ProductDetail from './ProductDetail'


export {
    Home,
    Login,
    ProductDetail
}